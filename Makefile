# $Ringlet$

PACKAGE?=	sstamp
VERSION?=	0.01

SCRIPTS?=	sstamp
MAN1?=		sstamp.1.gz

PREFIX?=	/usr
BINDIR?=	${PREFIX}/bin
SHAREDIR?=	${PREFIX}/share
MANDIR?=	${PREFIX}/share/man/man
ETCDIR?=	/etc
ETCHOSTERDIR?=	${ETCDIR}/hoster
INITDIR?=	${ETCDIR}/init.d
DOCSDIR?=	${SHAREDIR}/doc/${PACKAGE}
EXAMPLESDIR?=	${DOCSDIR}/examples

DISTEXT?=	.sample

CP?=		cp
ECHO?=		echo
GZIP?=		gzip -c9
INSTALL?=	install
MKDIR?=		mkdir -p
RM?=		rm -f

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	444

COPY?=		-c
STRIP?=		-s
INSTALL_PROGRAM?=	${INSTALL} ${COPY} ${STRIP} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_SCRIPT?=	${INSTALL} ${COPY} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=	${INSTALL} ${COPY} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

all:		${SCRIPTS} ${MAN1}

%:		%.pl
		${CP} $< $@

%:		%.sh
		${CP} $< $@

%.1.gz:		%.1
		${GZIP} $< > $@

install:	all
		${MKDIR} ${DESTDIR}${BINDIR}
		${INSTALL_SCRIPT} ${SCRIPTS} ${DESTDIR}${BINDIR}
		${MKDIR} ${DESTDIR}${MANDIR}1
		${INSTALL_DATA} ${MAN1} ${DESTDIR}${MANDIR}1

clean:
		${RM} ${SCRIPTS} ${MAN1}
