.\" Copyright (c) 2009, 2010  Peter Pentchev
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
.\" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\" $Ringlet$
.\"
.Dd April 16, 2010
.Dt SSTAMP 1
.Os
.Sh NAME
.Nm sstamp
.Nd record and restore file timestamps in a shadow tree
.Sh SYNOPSIS
.Nm
.Op Fl Nv
.Cm create
.Nm
.Op Fl Nv
.Cm add
.Ar filename...
.Nm
.Op Fl Nv
.Cm diff
.Ar filename...
.Nm
.Op Fl Nv
.Cm files
.Nm
.Op Fl Nv
.Cm remove
.Ar filename...
.Nm
.Op Fl Nv
.Cm update
.Nm
.Cm help
.Nm
.Cm version
.Sh DESCRIPTION
The
.Nm
.Dq ( shadow stamp )
utility keeps copies of certain files to preserve their timestamp.
This is useful in a development tree where patches are applied and
reverted, and a build tool such as
.Xr make 1
may get confused by files that may seem modified, but actually are not;
see the example below for details.
.Pp
The
.Nm
utility accepts the following options for its commands:
.Bl -tag -width indent
.It Fl N
No-operation mode - do not perform any changes to files in either
the current directory or the shadow tree, simply report what would
have been done.
.It Fl v
Verbose mode - display information about the actions performed.
.El
.Pp
The
.Nm
utility may be invoked with one of the following commands:
.Bl -tag -width indent
.It Cm create
Create an empty shadow tree for keeping track of files' timestamps.
The tree is created in a directory under
.Pa /var/db/sstamp
with a name formed by the real path of the current directory; thus,
the shadow tree for
.Pa /usr/src
would be in
.Pa /var/db/sstamp/usr/src .
.It Cm add
Copy one or more files into the shadow tree to preserve both their contents
and timestamps.
The file names must be specified as paths relative to the current
directory.
.It Cm diff
Show the differences between the shadow tree copies and the ones in
the current directory.
.It Cm files
List the files stored in the shadow tree for the current directory.
.It Cm remove
Remove one or more files from the shadow tree.
.It Cm update
Restore the timestamps of files in the current directory from
their copies stored in the shadow tree.
Before modifying a file's timestamp,
.Nm
compares the actual contents of the file with its stored copy and
exits with an error message if any differences are encountered;
it would probably be a mistake to put an old timestamp on a file that
has really been modified.
.It Cm help
Display program usage information and exit.
.It Cm version
Display program version information and exit.
.El
.Sh ENVIRONMENT
The
.Cm diff
command uses the following environment variables if present:
.Bl -tag -width
.It Ev DIFF
(default
.Sq diff )
The path to the
.Xr diff 1
command.
.It Ev DIFF_ARGS
(default
.Sq Fl u )
The arguments to pass to the
.Xr diff 1
command.
.El
.Sh FILES
.Bl -tag -width Ds -compact
.It Pa /var/db/sstamp
The root directory for maintaining the shadow trees.
.El
.Sh EXAMPLES
Consider a third-party source tree with local patches; the tree is
periodically updated from the external source, but before that update,
the local patches must be reverted only to be reapplied afterwards.
In this case, the
.Nm
utility could come useful:
.Bl -bullet -width indent
.It
Check out the source tree and apply the local patches.
.It
Create the shadow tree:
.Pp
.Dl sstamp create
.It
Store the timestamps of the patched files:
.Pp
.Dl sstamp add usr.bin/make/make.1 usr.bin/make/make.c
.It
Before updating, remove the local patches.
.It
Update the tree from the external source.
.It
Reapply the local patches, modifying the timestamps of the affected files.
.It
Restore the timestamps:
.Pp
.Dl sstamp update
.It
If
.Nm
detects that a file has been modified, refresh its copy in the shadow tree:
.Pp
.Dl sstamp add usr.bin/make/make.1
.It
Restore the timestamps, hopefully cleanly now :)
.Pp
.Dl sstamp update
.It
Run the build tool without unnecessary rebuilding simply because of
changed timestamps.
.El
.Sh DIAGNOSTICS
.Ex -std
.Sh SEE ALSO
.Xr cmp 1 ,
.Xr touch 1
.Sh STANDARDS
No standards were harmed during the production of the
.Nm
utility.
.Sh HISTORY
The
.Nm
utility was written by Peter Pentchev in 2009.
.Sh AUTHORS
.An Peter Pentchev
.Aq roam@ringlet.net
.Sh BUGS
No, thank you :)
But if you should actually find any, please report them
to the author.
