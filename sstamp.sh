#!/bin/sh
#
# Copyright (c) 2009, 2010  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# $Ringlet$

set -e

version()
{
	echo 'sstamp 0.01'
}

usage()
{
	cat <<EOUSAGE
Usage:	sstamp [-Nv] create
	sstamp [-Nv] add filename...
	sstamp [-Nv] diff filename...
	sstamp [-Nv] files
	sstamp [-Nv] remove filename...
	sstamp [-Nv] update
	sstamp help
	sstamp version

Options:
	-N	no-op mode - do not perform any changes;
	-v	verbose operation - display diagnostic messages.
EOUSAGE
}

sstamp_create()
{
	shift
	if [ "$#" -gt 0 ]; then
		echo 'The "create" command does not accept any arguments' 1>&2
		exit 1
	fi

	$NOOP mkdir $V -p "$SST_WORKDIR"
}

sstamp_add()
{
	shift
	if [ "$#" -lt 1 ]; then
		echo 'The "add" command needs at least one filename' 1>&2
		exit 1
	fi

	while [ "$#" -ge 1 ]; do
		# FIXME: check for a relative path with no ".." components
		d=`dirname "$1"`
		$NOOP mkdir -p $V "$SST_WORKDIR"/"$d"
		$NOOP cp -p $V "$1" "$SST_WORKDIR"/"$1"
		shift
	done
}

sstamp_diff()
{
	shift
	if [ "$#" -lt 1 ]; then
		echo 'The "diff" command needs at least one filename' 1>&2
		exit 1
	fi

	while [ "$#" -ge 1 ]; do
		# FIXME: check for a relative path with no ".." components
		$NOOP $DIFF $DIFF_ARGS "$SST_WORKDIR"/"$1" "$1"
		shift
	done
}

sstamp_files()
{
	shift
	if [ "$#" -gt 0 ]; then
		echo 'The "files" command does not accept any arguments' 1>&2
		exit 1
	fi

	if [ -n "$NOOP" ]; then
		$NOOP "cd \"$SST_WORKDIR\" && find . -type f"
		exit 0
	fi

	# TODO: Think of some way to make this verbose :)
	(cd "$SST_WORKDIR" && find . -type f | sed -e 's#^./##' | sort)
}

sstamp_remove()
{
	shift
	if [ "$#" -lt 1 ]; then
		echo 'The "remove" command needs at least one filename' 1>&2
		exit 1
	fi

	while [ "$#" -ge 1 ]; do
		# FIXME: check for a relative path with no ".." components
		$NOOP rm $V "$SST_WORKDIR"/"$1"
		shift
	done
}

sstamp_update()
{
	shift
	if [ "$#" -gt 0 ]; then
		echo 'The "update" command does not accept any arguments' 1>&2
		exit 1
	fi

	(cd "$SST_WORKDIR" && find . -type f) | while read f; do
		if [ ! -f "$f" ]; then
			echo "Disappeared file: $f" 1>&2
			exit 1
		fi
		if ! cmp -s "$SST_WORKDIR"/"$f" "$f"; then
			echo "MODIFIED file $f" 1>&2
			exit 1
		fi
		[ -n "$V" ] && echo "touch -r $SST_WORKDIR/$f $f"
		$NOOP touch -r "$SST_WORKDIR"/"$f" "$f"
	done
}

unset opt_v
unset NOOP V

SST_BASEDIR='/var/db/sstamp'

while getopts 'Nv' o; do
	case "$o" in
		N)
			opt_N=1
			;;
		v)
			opt_v=1
			;;
		*)
			usage 1>&2
			exit 1
	esac
done

shift `expr "$OPTIND" - 1`
if [ "$#" -lt 1 ]; then
	usage 1>&2
	exit 1
fi

# FIXME: Read a config file if specified

DIFF=${DIFF-'diff'}
DIFF_ARGS=${DIFF_ARGS-'-u'}

CWD="`pwd -P`"
SST_WORKDIR="$SST_BASEDIR/$CWD"

[ -n "$opt_N" ] && NOOP='echo'
[ -n "$opt_v" ] && V='-v'

case "$1" in
	create)
		sstamp_create "$@"
		;;
	add)
		sstamp_add "$@"
		;;
	diff)
		sstamp_diff "$@"
		;;
	help)
		usage
		;;
	files)
		sstamp_files "$@"
		;;
	remove)
		sstamp_remove "$@"
		;;
	update)
		sstamp_update "$@"
		;;
	version)
		version
		;;
	*)
		echo "Invalid command $1"
		usage 1>&2
		exit 1
		;;
esac
